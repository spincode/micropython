import React, { useState } from "react";
import PropTypes from "prop-types";

import { load } from "../../editor/load";

import styles from "./Harness.module.scss";

const redirectToVersion = ({ replace, setEditor, version }) => {
  replace(`/v/${version.major}/${version.minor}`);
  setEditor();
};

const Harness = ({
  history: { replace },
  match: {
    params: { major, minor }
  }
}) => {
  const [code, setCode] = useState("");
  const [editor, setEditor] = useState();

  const onChange = event => setCode(event.target.value);

  const onLoad = ({ code, version }) => {
    if (major !== version.major || minor !== version.minor)
      redirectToVersion({ version, replace, setEditor });

    setCode(code);
  };

  const Editor = editor ? editor.default : () => null;

  if (!editor) load(major, minor).then(module => setEditor(module));

  return (
    <section className={styles.harness}>
      <Editor onChange={onChange} onLoad={onLoad} code={code} />
    </section>
  );
};

Harness.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired
  }),
  match: PropTypes.shape({
    params: PropTypes.shape({
      major: PropTypes.string,
      minor: PropTypes.string
    })
  })
};

export default Harness;
