import React, { useRef } from "react";
import PropTypes from "prop-types";

import Button from "../Button";

const { alert } = window;

const handleLoadEnd = (onLoad, reader) => () => {
  try {
    const lines = reader.result.split(/\r?\n/);
    if (lines.length < 2)
      throw new Error("File must contain at least two lines!");
    const [version] = lines;
    const [major, minor] = version.split(".");
    const code = lines.slice(1).join("\n");

    onLoad({ version: { major, minor }, code });
  } catch (error) {
    console.error(error);
    alert("Something seems wrong with the contents of that file! ¯\\_(ツ)_/¯");
  }
};

const handleFileChange = onLoad => event => {
  const file = event.target.files[0];
  const reader = new FileReader();
  reader.readAsText(file);
  reader.onloadend = handleLoadEnd(onLoad, reader);
};

const LoadButton = ({ onLoad, style }) => {
  const fileEl = useRef(null);
  const onClick = () => fileEl.current.click();

  return (
    <>
      <input
        onChange={handleFileChange(onLoad)}
        ref={fileEl}
        style={{ display: "none" }}
        type="file"
      />
      <Button style={style} onClick={onClick}>
        Load
      </Button>
    </>
  );
};

LoadButton.propTypes = {
  onLoad: PropTypes.func.isRequired,
  style: PropTypes.object
};

export default LoadButton;
