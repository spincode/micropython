import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => (
  <p>
    404 - You might want to look <Link to="/v/2/0">here</Link>
  </p>
);

export default NotFound;
