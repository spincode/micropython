import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Harness from "../Harness";
import NotFound from "../NotFound";

import styles from "./App.module.scss";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className={styles.app}>
          <header className={styles.header}>
            <p>MicroPython Editor</p>
          </header>
          <main>
            <Switch>
              <Route path="/v/:major/:minor" component={Harness} />
              <Route path="/v/:major" component={Harness} />
              <Route path="/" component={NotFound} />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
