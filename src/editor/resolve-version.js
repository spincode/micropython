export const resolveVersion = (major = "x", minor = "x") => {
  const version = `${major}.${minor}`;
  switch (version) {
    case "1.x":
      return "1.1";
    case "2.x":
      return "2.0";
    case "x.x":
      return "2.0";
    default:
      return version;
  }
};
