import React from "react";

import LoadButton from "../../../components/LoadButton";
import SaveButton from "../../../components/SaveButton";

import styles from "./Editor.module.scss";

const buttonStyle = { backgroundColor: "pink", color: "maroon" };

const Editor = ({ code, onChange, onLoad }) => (
  <>
    <textarea className={styles.editor} value={code} onChange={onChange} />
    <div>
      <LoadButton onLoad={onLoad} style={buttonStyle} />
      <SaveButton style={buttonStyle} />
    </div>
    <p>v1.1</p>
  </>
);

export default Editor;
