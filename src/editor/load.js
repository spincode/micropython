import { resolveVersion } from "./resolve-version";

export const load = (major, minor) => {
  const version = resolveVersion(major, minor);
  return import(`./version/${version}/index.js`);
};
