# Python Deployment

## Pitfalls and Challenges

The main technical challenges presented by these requirements are:

- Maintaining multiple disparate versions of the Editor codebase in a convenient way that avoids duplication and allows minor/patch releases of a given version to be created when the need arises.

- Allowing deployment of a single new editor version, or re-deployment of all versions.

- Identifying which elements of the application will be shared unversioned and which comprise the editor and will be actively versioned.

- Invoking the appropriate editor for a given script, as determined by the version metdata.

- Creating unique shareable URLs for saved scripts

## Proposed Implementation

### Git Versioning

There will be two project repos:

- `main` - The main site scaffolding, layout, router and surrounding controls. This will be an evergreen version that is deployed to the root of the domain.

- `editor` - The editor iself comprising the editing area and supporting controls.

The `editor` repository will contain branches for each major version, off which feature branches will be created, new features implemented, PR reviewed and then merged back to the major version branch with an appropriate release tag applied.

Metadata about available versions, compatibility and bugfixes will be maintained in the `main` repo.

### Application Design

The design is based on the principle that the main site and surrounding chrome will be a single-page application that invokes the appropriate editor by embedding it within the editor region of the page. The editor will be a separately maintained application that can be flexibly versioned. There are two main approaches possible:

1. Embedding the editor as a separate SPA within an `<iframe>`, using `postMessage` for signalling.

2. Building Editor component chunks using webpack code-splitting and dynamically loading with ES6 imports.

The first approach is the most straightforward from a build perspective and should present few problems, so long as webpack is configured with the correct `baseUrl` for the version.

The second approach is cleaner on the code level since it means that the editor can be passed props and/or be connected to the `main` parent application's redux store. It is more complex from a build perspective, however, since it would require careful webpack configuration to ensure chunk naming and loading across multiple builds works as expected. A POC might be worthwhile since this would be a very elegant approach.

The responsibility for loading and saving scripts will belong to the `main` repo, since it is in control of the URL and is best placed to make decisions about which version of the editor is to be used. This functionality can be triggered by controls in the editor if required.

The shareable URL functionality does require some server-side assistance to ensure that storage is not abused, for instance by a DoS attack or being repurposed for another application. Monitoring and alerts on spikes of storage would be desirable and potentially limiting / throttling storage by IP address, though this is complicated by NAT. Ultimately I would expect scripts to be stored in a separate repository, likely in a document storage database such as Google Cloud FireStore, where a unique key could be assigned and used in the share URL.

To limit abuse, it would be worth implementing a CAPTCHA-type prompt before creating the URL. For example: https://www.google.com/recaptcha/intro/v3.html

### Build and Deployment

Steps involved:

1. Checkout of the `main` repo, production build and deployment of files to the main file repository serving `python.microbit.org` e.g. Amazon S3 bucket / Google Cloud.

2. By reference to the version metadata in the `main` repo from step 1, each branch of `editor` will be checked out at the appropriate tag commit and built. The build files will be deployed to `/editor/v/{version}` in the aforementioned file repo.

Step 2 could optionally be a single named version, for incremental deployment of a single new editor version.

### Testing Approach

Testing can be approached at these levels:

1. Unit tests - Using shallow-rendering via `enzyme` and test-runner `jest` or `karma`, with `sinon` for stubbing (mainly applicable to karma). Local to `main` or `editor` repo.

2. Component integration tests - Typically `jest` / `karma` tests with fully mounted components, to ensure that module contracts are being correctly adhered to. Local to `main` or `editor` repo.

3. Functional tests - Test user journeys through the application, inspecting the page at each point to ensure that the expected elements occur. Requires a fully functional environment and verifies that the integration between `main` and `editor` is correct.

I would expect that the above tests would involve a curated library of scripts for each version of the editor to ensure that no regression has occurred.

# Sample Code

## Important Notes

- The sample code created for this exercise does not closely follow the recommended approach above since I would not have had time to present a sufficiently working solution. I have therefore concentrated on the loading of script files into the editor component, parsing the version and conditionally loading another version of the editor if required.

- The sample uses code-splitting via ES6 dynamic imports to load individual editor components on-demand, meaning that all three editor versions are maintained in the same codebase, solely for the purposes of this demo.

- Unit tests have not been implemented, again due to time constraints. In practise I would like to see the code fully unit-tested.

## How to run

In the project directory, run `yarn` or `npm install` followed by:

### `yarn start` or `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build` or `npm run build`

Builds the app for production to the `build` folder, including chunks for editor versions<br>
